#include <scene.hpp>
#include <fstream>

namespace mt
{
	Scene::Scene(int width, int height)
	{
		m_width = width;
		m_height = height;
		m_window = std::make_unique<sf::RenderWindow>(sf::VideoMode(m_width, m_height), "lab_13");
		m_texture = std::make_unique<sf::Texture>();
		m_texture->create(m_width, m_height);
		m_sprite = std::make_unique<sf::Sprite>(*m_texture);
	}

	Scene::~Scene()
	{
		if (m_points != nullptr)
			delete[] m_points;
	}

	double Scene::Speed_y()
	{ 
		if (m_speed_y > 0)
			return m_speed_y +=0.05;
		return 0; 
	}
	
	bool Scene::Pro_Input(std::string filename)
	{
		std::ifstream in(filename);
		if (!in)
		{
			std::cout << "ERR: File not found " << filename << std::endl;
			exit(-1);
		}
	}

	void Scene::Input(Pixel* pixel, Point* points, std::string filename)
	{
		// �������� �����
		Pro_Input(filename);
		
		std::ifstream in(filename);
		Point position = { 467340.0, -60.0, 6063520.0 };
		Angles angles = { 0.0,1.8,0.0 };
		Intrinsic intrinsic = { 960.0, 540.0, 960.0, 540.0 };
		m_camera = std::make_unique<Camera>(m_width, m_height, intrinsic, position, angles);

		m_size = 0;
		int r = 0, g = 0, b = 0;
		while (!in.eof())
		{
			in >> points[m_size].x >> points[m_size].y >> points[m_size].z >> r >> g >> b;
			pixel[m_size].r = r;
			pixel[m_size].g = g;
			pixel[m_size].b = b;
			m_size++;
		}
		in.close();
	}


	void Scene::Sphere(Pixel* pixel ,Point* points)
	{
		Intrinsic intrinsic = { 960.0, 540.0, 960.0, 540.0 };
		Point position = { 0.0, 0.0, 0.0 };
		Angles angles = { 0.0,0.0,0.0 };
		m_camera = std::make_unique<Camera>(m_width, m_height, intrinsic, position, angles);

		m_speed_y = 0.1;
		m_size = 0;
		double r = 1;
		for (double fi = 0; fi < 6.28; fi += 0.01)
			for (double teta = 0; teta < 1.57; teta += 0.01)
			{
				points[m_size].x = r * sin(teta) * cos(fi);
				points[m_size].y = r * sin(teta) * sin(fi);
				points[m_size].z = r * cos(teta);
				pixel[m_size].r = 255;
				pixel[m_size].g = 0;
				pixel[m_size].b = 0;
				pixel[m_size].a = 255;
				m_size++;
			}
	}

	void Scene::LifeCycle()
	{
		m_points = new Point[400000];
		Point* points = new Point[400000];
		Pixel* pixels = new Pixel[400000];
		double y0 = 0;

		//Sphere(pixels, points);

		Input(pixels, points, "points.txt");

		while (m_window->isOpen()) {
			sf::Event event;
			while (m_window->pollEvent(event))
				if (event.type == sf::Event::Closed)
					m_window->close();

			if (sf::Keyboard::isKeyPressed(sf::Keyboard::W))
			{
				m_camera->dZ(0.1);
			}
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::S))
			{
				m_camera->dZ(-0.1);
			}
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::A))
			{
				m_camera->dX(-0.1);
			}
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::D))
			{
				m_camera->dX(0.1);
			}
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left))
			{
				m_camera->dPitch(-0.02);
			}
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::Right))
			{
				m_camera->dPitch(0.02);
			}
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::Up))
			{
				m_camera->dRoll(-0.02);
			}
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::Down))
			{
				m_camera->dRoll(0.02);
			}
			
			int k = m_size;
			m_size = 0;
			y0= Speed_y();
			for (int i = 0; i < k; i++)
			{
				m_points[m_size].x = points[i].x;
				m_points[m_size].y = points[i].y + y0;
				m_points[m_size].z = points[i].z;
				m_size++;
			}

			// ������������� �����
			for (int i = 0; i < m_size; i++)
				m_camera->ProjectPoint(m_points[i], { pixels[i].r,pixels[i].g,pixels[i].b,255 });

			m_texture->update((uint8_t*)m_camera->Picture(), m_width, m_height, 0, 0);
			m_camera->Clear();


			m_window->clear();
			m_window->draw(*m_sprite);

			m_window->display();

		}
	}
}